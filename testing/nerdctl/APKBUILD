# Contributor: psykose <alice@ayaya.dev>
# Maintainer: psykose <alice@ayaya.dev>
pkgname=nerdctl
pkgver=0.23.0
pkgrel=2
pkgdesc="Docker-compatible CLI for containerd"
url="https://github.com/containerd/nerdctl/"
arch="all"
license="Apache-2.0"
depends="ca-certificates containerd cni-plugins iptables ip6tables"
makedepends="go"
subpackages="
	$pkgname-doc
	$pkgname-bash-completion
	$pkgname-fish-completion
	$pkgname-zsh-completion
	"
source="$pkgname-$pkgver.tar.gz::https://github.com/containerd/nerdctl/archive/refs/tags/v$pkgver.tar.gz"
options="!check" # a lot fail

export GOFLAGS="$GOFLAGS -modcacherw -trimpath"
export GOCACHE="${GOCACHE:-"$srcdir/go-cache"}"
export GOTMPDIR="${GOTMPDIR:-"$srcdir"}"
export GOMODCACHE="${GOMODCACHE:-"$srcdir/go"}"

build() {
	go build -ldflags "-X github.com/containerd/nerdctl/pkg/version.Version=$pkgver" \
		-o nerdctl ./cmd/nerdctl

	for shell in bash fish zsh; do
		./nerdctl completion $shell > nerdctl.$shell
	done
}

package() {
	install -Dm755 nerdctl -t "$pkgdir"/usr/bin
	install -Dm644 docs/*.md -t "$pkgdir"/usr/share/doc/$pkgname

	install -Dm644 nerdctl.bash \
		"$pkgdir"/usr/share/bash-completion/completions/nerdctl
	install -Dm644 nerdctl.fish \
		"$pkgdir"/usr/share/fish/completions/nerdctl.fish
	install -Dm644 nerdctl.zsh \
		"$pkgdir"/usr/share/zsh/site-functions/_nerdctl
}

sha512sums="
2d3a268cde5e4ef02172f450690d7a8810111e725d1f35a707267cb8e04b64cec4329df0878997dfcd980a73b9dd297ac97f554f37cc7df5159d581e372bdc17  nerdctl-0.23.0.tar.gz
"
