# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=aura-browser
pkgver=5.26.0
pkgrel=0
pkgdesc="Browser for a fully immersed Big Screen experience allowing you to navigate the world wide web using just your remote control"
url="https://invent.kde.org/plasma-bigscreen/aura-browser"
# armhf blocked by extra-cmake-modules
# s390x, ppc64le and riscv64 blocked by qt5-qtwebengine
arch="all !armhf !s390x !ppc64le !riscv64"
license="GPL-2.0-or-later"
depends="
	kirigami2
	qt5-qtvirtualkeyboard
	"
makedepends="
	extra-cmake-modules
	ki18n-dev
	kirigami2-dev
	qt5-qtbase-dev
	qt5-qtdeclarative-dev
	qt5-qtmultimedia-dev
	qt5-qtquickcontrols2-dev
	qt5-qtwebengine-dev
	samurai
	"
case "$pkgver" in
	*.90*) _rel=unstable;;
	*) _rel=stable;;
esac
source="https://download.kde.org/$_rel/plasma/$pkgver/aura-browser-$pkgver.tar.xz"
options="!check" # No tests

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=MinSizeRel \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
624a3365211d4e53ba48c15ad10102f3622be9bcf16e17e471587de240f7f57c050fe445bbebd7f3406a372ae07159f9596f5fe6bda5e988ba2b8445dce7ff35  aura-browser-5.26.0.tar.xz
"
