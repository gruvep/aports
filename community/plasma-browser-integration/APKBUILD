# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=plasma-browser-integration
pkgver=5.26.0
pkgrel=0
pkgdesc="Components necessary to integrate browsers into the Plasma Desktop"
# armhf blocked by extra-cmake-modules
# ppc64le, s390x and riscv64 blocked by qt5-qtwebengine -> purpose
arch="all !armhf !ppc64le !s390x !riscv64"
url="https://community.kde.org/Plasma/Browser_Integration"
license="GPL-3.0-or-later"
makedepends="
	extra-cmake-modules
	kactivities-dev
	kconfig-dev
	kdbusaddons-dev
	kfilemetadata-dev
	ki18n-dev
	kio-dev
	knotifications-dev
	krunner-dev
	plasma-workspace-dev
	purpose-dev
	qt5-qtbase-dev
	samurai
	"

case "$pkgver" in
	*.90*) _rel=unstable;;
	*) _rel=stable;;
esac
source="https://download.kde.org/$_rel/plasma/$pkgver/plasma-browser-integration-$pkgver.tar.xz"
subpackages="$pkgname-lang"

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	cd build
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
7a94635b9eead8a34bdaebd6dc2fbae06d868951303df83e312ab14246300bd359fc4fa73a5140e0b39d36ba721703aacb3d6ac33b94bbf3b7f9f3f9c47a2707  plasma-browser-integration-5.26.0.tar.xz
"
