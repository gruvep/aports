# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=systemsettings
pkgver=5.26.0
pkgrel=0
pkgdesc="Plasma system manager for hardware, software, and workspaces"
# armhf blocked by qt5-qtdeclarative
arch="all !armhf"
url="https://kde.org/plasma-desktop/"
license="GPL-2.0-or-later"
depends="kirigami2"
makedepends="
	extra-cmake-modules
	kactivities-dev
	kactivities-stats-dev
	kcmutils-dev
	kconfig-dev
	kcrash-dev
	kdbusaddons-dev
	kdeclarative-dev
	kdoctools-dev
	khtml-dev
	ki18n-dev
	kiconthemes-dev
	kio-dev
	kirigami2-dev
	kitemviews-dev
	kpackage-dev
	kservice-dev
	kwidgetsaddons-dev
	kwindowsystem-dev
	kxmlgui-dev
	plasma-workspace-dev
	qt5-qtbase-dev
	qt5-qtdeclarative-dev
	samurai
	"

case "$pkgver" in
	*.90*) _rel=unstable;;
	*) _rel=stable;;
esac
source="https://download.kde.org/$_rel/plasma/$pkgver/systemsettings-$pkgver.tar.xz"
subpackages="$pkgname-doc $pkgname-lang"

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	cd build
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
4fc7d3c58010ce30cdf93d32128fabab3e5d95148f6a4ed3b43dc81453a64e8131507c1a4dd0055a0979bf35733068771a07a0c9fe69a427c868d06bdf95bdc2  systemsettings-5.26.0.tar.xz
"
